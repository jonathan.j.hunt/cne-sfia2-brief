ssh -i /home/jenkins/JonathanQA.pem ubuntu@*TESTVM-IP* << EOF

sudo docker rmi --force jonathanjhunt/backend
sudo docker rmi --force jonathanjhunt/frontend
##removes previous git project
sudo rm -r /home/ubuntu/cne-sfia2-brief
##clone current project directory
cd /home/ubuntu
sudo git clone https://gitlab.com/jonathan.j.hunt/cne-sfia2-brief.git
#cd into project
cd /home/ubuntu/cne-sfia2-brief

#docker compose (build web server)
sudo docker-compose up -d --build

#run tests
docker exec backend bash -c "pytest tests/ --cov application"
docker exec frontend bash -c "pytest tests/ --cov application"

#shut down webserver
docker-compose down

EOF