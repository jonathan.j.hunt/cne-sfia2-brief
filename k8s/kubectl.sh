kubectl create ns project
cd /home/jenkins/cne-sfia2-brief/k8s
kubectly apply -f secret.yaml -n project
kubectl apply -f nginx-cm.yaml
kubectl delete deployment frontend.yaml -n project
kubectl apply -f frontend.yaml
kubectl apply -f frontend-cip.yaml
kubectl apply -f backend.yaml
kubectl apply -f backend-cip.yaml
kubectl apply -f nginx.yaml
kubectl apply -f nginx-lb.yaml
kubectl get svc -n project
